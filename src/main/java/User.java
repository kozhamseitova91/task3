import java.io.File;
import java.util.Scanner;

public class User {
    private int id;
    private static int gen_id = 0;
    private String name;
    private String surname;
    private String username;
    private Password password;
    private String major;

    public User(){
        generate_id();
    }

    public User(int i, String name, String surname, String username, String s1, String major) {
        id = i;
        this.setName(name);
        this.setSurname(surname);
        this.setUsername(username);
        setPassword(new Password(s1));
        this.setMajor(major);
    }


    public User(String name, String surname, String username, String password, String major) {
        this();
        this.setName(name);
        this.setSurname(surname);
        this.setUsername(username);
        this.setPassword(new Password(password));
        this.setMajor(major);
    }

    public void generate_id() {
        readId();
        this.id = gen_id++;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
        System.out.print(name + " ");
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
        System.out.print(surname + " ");
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        if (this.checkUsername(username)) {
            this.username = username;
        } else {
            System.out.println("User.setUsername: Incorrect username");
        }
    }

    public String getPassword() {
        return this.password.getPasswordStr();
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

     public boolean checkUsername(String username) {
        int len = username.length();
        return len > 4 && Character.isLetter(username.charAt(0));
    }

    private void readId()  {
        try{
            File file = new File("C:\\Users\\Acer\\IdeaProjects\\Assignment 2\\src\\com\\company\\db.txt");
            Scanner sc = new Scanner(file);
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                String[] data = line.split(" ");
                gen_id = Integer.parseInt(data[0]);
            }
            ++gen_id;
        }
        catch(Exception i){
            System.out.println("User.readId : ERROR!!!" + i);
        }
    }
    @Override
    public String toString() {
        return id + " " + name + " " + surname + " " + username + " " + password.toString() + " " + major;
    }
}
