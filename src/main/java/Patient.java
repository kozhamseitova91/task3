import java.util.Date;

public class Patient extends User {

    private ComingDay comingDay;
    private ComingTime comingTime;
    private Illnesses illness;
    public Patient(int id, String datum, String surname, String username, String password, String major, String s, String datum1, String s1){

    }

    public Illnesses getIllness() {
        return illness;
    }

    public void setIllness(Illnesses illness) {
        this.illness = illness;
    }

    public ComingDay getComingDay() {
        return comingDay;
    }

    public void setComingDay(ComingDay comingDay) {
        this.comingDay = comingDay;
    }

    public ComingTime getComingTime(){
        return comingTime;
    }

    public void setComingTime(ComingTime comingTime) {
        this.comingTime = comingTime;
    }

    protected Patient(int id, String name, String surname, String username, String password, String major, Illnesses illness, ComingDay comingDay, ComingTime comingTime){
        super(id, name, surname, username, password, major );
        setIllness(illness);
        setComingDay(comingDay);
        setComingTime(comingTime);
    }

    Patient(String name, String surname, String username, String password, String major, Illnesses illness, ComingDay comingDay, ComingTime comingTime){
        super(name, surname, username, password, major );
        setIllness(illness);
        setComingDay(comingDay);
        setComingTime(comingTime);
    }

    @Override
    public String toString() {
        return getId() + " " + getName() + " " + getSurname() + " " + getUsername() + " " + getPassword() + " " + getMajor() + " " + getIllness() + " " + getComingDay() + " " + getComingTime();
    }
}
