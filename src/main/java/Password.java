public class Password {
    private String passwordStr;

    public Password() {
    }

    public Password(String passwordStr) {

        this.setPasswordStr(passwordStr);
    }

    public String getPasswordStr() {

        return this.passwordStr;
    }

    public void setPasswordStr(String passwordStr) {
        if (checkPassword(passwordStr)) {
            this.passwordStr = passwordStr;
            System.out.print(passwordStr);
        } else {
            System.out.println("Password.setPasswordStr: Incorrect password");
            System.out.print(passwordStr);
        }

    }

    private static boolean checkPassword(String passwordStr) {
        int count = 0;
        int count1 = 0;
        int count2 = 0;
        if (passwordStr.length() > 9) {
            for(int i = 0; i < passwordStr.length(); ++i) {
                if (Character.isDigit(passwordStr.charAt(i))) {
                    ++count;
                } else if (Character.isLetter(passwordStr.charAt(i))) {
                    if (Character.isLowerCase(passwordStr.charAt(i))) {
                        ++count1;
                    } else if (Character.isUpperCase(passwordStr.charAt(i))) {
                        ++count2;
                    }
                }
            }
            if (count > 0 && count1 > 0 && count2 > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return  passwordStr;
    }
}
