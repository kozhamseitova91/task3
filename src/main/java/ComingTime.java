public enum ComingTime {
    A ("8:00-8:25"),
    B ("8:30-8:55"),
    C ("9:00-9:25"),
    D ("9:30-9:55"),
    E ("10:00-10:25"),
    F ("10:30-10:55"),
    G ("11:00-11:25"),
    H ("11:30-11:55"),
    I ("12:00-12:25"),
    J ("12:30-12:55");

    private String comingTime;

    ComingTime(String comingTime) {
        this.comingTime=comingTime;
    }

    public String getComingTime() {
        return comingTime;
    }


    @Override
    public String toString() {
        return "ComingTime{" +
                "comingTime='" + comingTime + '\'' +
                '}';
    }
}
