public enum Illnesses {
    TOOTHACHE ("toothache"),
    ALLERGIES ("allergies"),
    GENERAL ("general"),
    DIARRHEA ("diarrhea");

    private String illness;

    Illnesses(String illness) {
        this.illness=illness;
    }

    public String getIllness() {
        return illness;
    }


    @Override
    public String toString() {
        return "Illnesses{" +
                "illness='" + illness + '\'' +
                '}';
    }
}
