public enum Specialities {
    DENTIST ("dentist"),
    ALLERGIST ("allergist"),
    THERAPIST ("therapist"),
    GASTRO ("gastroenterologist");

    private String speciality;

    Specialities(String speciality) {
        this.speciality=speciality;
    }

    public String getSpeciality() {
        return speciality;
    }


    @Override
    public String toString() {
        return "Specialities{" +
                "speciality='" + speciality + '\'' +
                '}';
    }
}
