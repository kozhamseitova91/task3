public enum ComingDay {
    MONDAY ("monday"),
    TUESDAY ("tuesday"),
    WEDNESDAY ("wednesday"),
    THURSDAY ("thursday"),
    FRIDAY ("friday");

    private String day;

    ComingDay(String day) {
        this.day=day;
    }

    public String getDay() {
        return day;
    }


    @Override
    public String toString() {
        return "ComingDay{" +
                "day='" + day + '\'' +
                '}';
    }
}
