import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class MyApplication {
    ArrayList<Doctor> doctors;
    ArrayList<Patient> patients;
    private Scanner sc = new Scanner(System.in);
    private Doctor signedDoctor;
    private Patient signedPatient;

    public MyApplication() {
        doctors = new ArrayList<>();
        patients = new ArrayList<>();
    }

    public void addDoctor(Doctor doctor) {

        this.doctors.add(doctor);
    }

    public void addPatient(Patient patient) {

        this.patients.add(patient);
    }

    private void menu() throws IOException {
        while(true) {
            if (signedDoctor == null && signedPatient == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int select = this.sc.nextInt();
                if (select == 1) {
                    authentication();
                }
            } else {
                if(signedDoctor != null){
                    ProfileDoctor();
                }
                else{
                    ProfilePatient();
                }
            }
        }
    }

    private void logOff() throws IOException {
        saveUserList();
        signedDoctor = null;
        signedPatient = null;
    }

    private void authentication() throws IOException {
                System.out.println("1. Sign in");
                System.out.println("2. Sign up");
                int select = sc.nextInt();
                if (select == 1) {
                    signIn();
                } else if (select == 2) {
                    signUp();
                }
        }

    private void signIn() throws IOException {
        System.out.println("Enter your username: ");
        String username = sc.next();
        System.out.println("Enter your password:");
        String passwordStr = sc.next();
        if (checkDoctor(username, passwordStr)) {
            signedDoctor = getDoctor(username, passwordStr);
            if(signedDoctor != null) {
                ProfileDoctor();
            }
            else{
                System.out.println("MyApplication.signIn: Incorrect username or password");
            }
        }
        else if(checkPatient(username, passwordStr)){
            signedPatient = getPatient(username, passwordStr);
            if(signedDoctor != null){
                ProfileDoctor();
            }
            else{
                System.out.println("MyApplication.signIn: Incorrect username or password");
            }
        }
    }

    public void ProfileDoctor() throws IOException {

            while(true) {
                System.out.println("1. Show profile");
                System.out.println("2. Log off");
                int select = sc.nextInt();
                if(select == 1) {
                    System.out.println(signedDoctor.getName() + " " + signedDoctor.getSurname() + " " + "You are online");
                    System.out.println(signedDoctor.getMajor() + ": " + signedDoctor.getSpeciality());
                }
                else {
                    logOff();
                    break;
                }
            }
    }

    public void ProfilePatient() throws IOException {

        while(true) {
            System.out.println("1. Show profile");
            System.out.println("2. Log off");
            int select = sc.nextInt();
            if(select == 1) {
                System.out.println(signedPatient.getName() + " " + signedPatient.getSurname() + " " + "You are online");
                System.out.println("Reason for visit" + ": " + signedPatient.getIllness());
                System.out.println("Date of coming:" + signedPatient.getComingDay());
                System.out.println("Time:" + signedPatient.getComingTime());
            }
            else {
                logOff();
                break;
            }
        }
    }

    private Doctor getDoctor(String username, String password) {
        Iterator var3 = doctors.iterator();

        Doctor doctor;
        do {
            if (!var3.hasNext()) {
                return null;
            }

            doctor = (Doctor)var3.next();
        } while(!doctor.getUsername().equals(username) || !doctor.getPassword().equals(password));

        return doctor;
    }

    private Patient getPatient(String username, String password) {
        Iterator var3 = patients.iterator();

        Patient patient;
        do {
            if (!var3.hasNext()) {
                return null;
            }

            patient = (Patient)var3.next();
        } while(!patient.getUsername().equals(username) || !patient.getPassword().equals(password));

        return patient;
    }

    private boolean checkDoctor(String username, String password) {
        for (Doctor doctor : doctors) {
            if (doctor.getUsername().equals(username) && doctor.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkPatient(String username, String password) {
        for(Patient patient: patients){
            if(patient.getUsername().equals(username)&&patient.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }


    private void signUp(){
        System.out.println("Please, enter your name: ");
        String name = this.sc.next();
        System.out.println("Enter your surname: ");
        String surname = this.sc.next();
        System.out.println("Create a username: ");
        String username = this.sc.next();
        System.out.println("Create a password: ");
        String password = this.sc.next();
        System.out.println("Select your major: ");
        String major = major();
        try {
            if(major == "doctor") {
                Specialities speciality = chooseSpeciality();
                signedDoctor = new Doctor(name, surname, username, password, major, speciality);
                addDoctor(signedDoctor);
                saveUserList();
            }
            else if (major == "patient"){
                Illnesses illness = chooseIllness();
                ComingDay comingDay = chooseDay();
                ComingTime comingTime = chooseTime();
                if(!checkDate(comingDay, comingTime)){
                    System.out.println("This time is already taken");
                }
                else{
                    signedPatient = new Patient (name, surname, username, password, major, illness, comingDay, comingTime);
                    addPatient(signedPatient);
                    saveUserList();

                }
            }
        } catch (Exception var7) {
            System.out.println(var7.getMessage());
        }


    }
    public String major(){
        System.out.println("1. Doctor");
        System.out.println("2. Patient");
        int select = this.sc.nextInt();
        if(select == 1){
            return "doctor";
        }
        else if(select == 2){
            return "patient";
        }
        return null;
    }

    public Specialities chooseSpeciality(){
        Specialities spec = null;
        System.out.println("1. dentist");
        System.out.println("2. allergist");
        System.out.println("3. therapist");
        System.out.println("4. gastroenterologist");
        int select = this.sc.nextInt();
        if(select == 1){
            return spec.DENTIST;
        }
        else if(select == 2){
            return spec.ALLERGIST;
        }
        else if(select == 3){
            return spec.THERAPIST;
        }
        else if(select == 4){
            return spec.GASTRO;
        }
        return null;
    }

    public Illnesses chooseIllness(){
        Illnesses illness = null;
        System.out.println("1. toothache");
        System.out.println("2. allergies");
        System.out.println("3. general");
        System.out.println("4. diarrhea");
        int select = this.sc.nextInt();
        if(select == 1){
            return illness.TOOTHACHE;
        }
        else if(select == 2){
            return illness.ALLERGIES;
        }
        else if(select == 3){
            return illness.GENERAL;
        }
        else if(select == 4){
            return illness.DIARRHEA;
        }
        return null;
    }

    public ComingDay chooseDay(){
        ComingDay day = null;
        System.out.println("You can only make an appointment for the next week!!!");
        System.out.println("Choose the day of admission:");
        System.out.println("1. Monday");
        System.out.println("2. Tuesday");
        System.out.println("3. Wednesday");
        System.out.println("4. Thursday");
        System.out.println("5. Friday");
        int select = this.sc.nextInt();
        if(select == 1){
            return day.MONDAY;
        }
        else if(select == 2){
            return day.TUESDAY;
        }
        else if(select == 3){
            return day.WEDNESDAY;
        }
        else if(select == 4){
            return day.THURSDAY;
        }
        else if(select == 5){
            return day.FRIDAY;
        }
        return null;
    }

    public ComingTime chooseTime(){
        ComingTime time = null;
        System.out.println("Choose the time of admission:");
        System.out.println("1. 8:00-8:25");
        System.out.println("2. 8:30-8:55");
        System.out.println("3. 9:00-9:25");
        System.out.println("4. 9:30-9:55");
        System.out.println("5. 10:00-10:25");
        System.out.println("6. 10:30-10:55");
        System.out.println("7. 11:00-11:25");
        System.out.println("8. 11:30-11:55");
        System.out.println("9. 12:00-12:25");
        System.out.println("0. 12:30-12:55");
        int select = this.sc.nextInt();
        if(select == 1){
            return time.A;
        }
        else if(select == 2){
            return time.B;
        }
        else if(select == 3){
            return time.C;
        }
        else if(select == 4){
            return time.D;
        }
        else if(select == 5){
            return time.E;
        }
        else if(select == 6){
            return time.F;
        }
        else if(select == 7){
            return time.G;
        }
        else if(select == 8){
            return time.H;
        }
        else if(select == 9){
            return time.I;
        }
        else if(select == 0){
            return time.J;
        }
        return null;
    }

    private Patient getDate(Enum comingDay, Enum comingTime) {
        Iterator var3 = patients.iterator();

        Patient patient;
        do {
            if (!var3.hasNext()) {
                return null;
            }

            patient = (Patient)var3.next();
        } while(!patient.getComingDay().equals(comingDay) || !patient.getComingTime().equals(comingTime));

        return patient;
    }

    public boolean checkDate(Enum comingDay, Enum comingTime){
        for(Patient patient: patients){
            if(patient.getComingDay().equals(comingDay)&&patient.getComingTime().equals(comingTime)) {
                return true;
            }
        }
        return false;
    }
    public void start() throws IOException {
        readFile();

        while(true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int select = this.sc.nextInt();
            if (select != 1) {
                return;
            }
            menu();
        }
    }

    private void readFile() throws FileNotFoundException {
        File file = new File("C:\\Users\\Acer\\IdeaProjects\\Assignment 2\\src\\com\\company\\db.txt");
        Scanner fileScanner = new Scanner(file);

        while(fileScanner.hasNextLine()) {
            String line = fileScanner.nextLine();
            String[] data = line.split(" ");
            Doctor dd = new Doctor(Integer.parseInt(data[0]), data[1], data[2], data[3], data[4], data[5], data[6]);
            addDoctor(dd);
        }

        File file1 = new File("C:\\Users\\Acer\\Desktop\\dbb.txt");
        Scanner fileScanner1 = new Scanner(file1);

        while(fileScanner1.hasNextLine()) {
            String line = fileScanner1.nextLine();
            String[] data = line.split(" ");
            Patient patient = new Patient(Integer.parseInt(data[0]), data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8]);
            addPatient(patient);
        }

    }

    private void saveUserList() throws IOException {
        String content = "";
        for (Doctor doctor: doctors){
            content+= doctor + "\n";
        }
        Files.write(Paths.get("C:\\Users\\Acer\\IdeaProjects\\Assignment 2\\src\\com\\company\\db.txt"), content.getBytes());

        for (Patient patient: patients){
            content+= patient + "\n";
        }
        Files.write(Paths.get("C:\\Users\\Acer\\Desktop\\dbb.txt"), content.getBytes());
    }
}
