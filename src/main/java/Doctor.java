public class Doctor extends User {
    private Specialities speciality;

    public Doctor(int id, String datum, String surname, String username, String password, String major, String s){

    }

    public Specialities getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Specialities speciality) {
        this.speciality = speciality;
    }
    protected Doctor(int id, String name, String surname, String username, String password, String major, Specialities speciality){
        super(id, name, surname, username, password, major );
        setSpeciality(speciality);

    }

    public Doctor(String name, String surname, String username, String password, String major,  Specialities speciality){
        super(name, surname, username, password, major);
        setSpeciality(speciality);
    }


    @Override
    public String toString() {
        return getId() + " " + getName() + " " + getSurname() + " " + getUsername() + " " + getPassword() + " " + getMajor() + " " + getSpeciality();
    }
}

